cat << 'EOF' >> /home/vagrant/.bashrc
export JAVA_HOME=/usr/lib/jvm/java-7-oracle/

export ANDROID_HOME=/vagrant/android-sdk-linux
export ANDROID_SDK=$ANDROID_HOME

export APPIUM_HOME=/usr/local/lib/node_modules/appium

PATH=$PATH:/usr/local/share/npm/bin/
PATH=$PATH:$ANDROID_HOME/build-tools
PATH=$PATH:$ANDROID_HOME/platform-tools
PATH=$PATH:$ANDROID_HOME/tools
EOF

echo 'cd /vagrant' >> /home/vagrant/.bashrc